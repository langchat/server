const mongoose = require('mongoose');
const auth = require('../auth')
const User = mongoose.model('User');
const Thread = mongoose.model('Thread');
const Message = mongoose.model('Message');

const router = require('express').Router();

router.get('/threads', auth.required, (req, res, next) => Thread.find({ users: { $in: [req.user.id] } })
  .then(threads => Promise.all(threads.map(thread => User.find({ _id: { $in: thread.users } })
    .then((users) => {
      Object.assign(thread, { users })
      if (!thread.title) {
        const title = req.user.id === users[0]._id ? users[0].username : users[1].username
        Object.assign(thread, { title })
      }
      return thread;
    }))))
  .then(threads => res.json({ data: threads }))
  .catch(next));

router.get('/thread', (req, res, next) => {
  if (req.query.uid) {
    User.findById(req.query.uid, (err, user) => {
      if (err) throw err;
      Thread.findOneAndUpdate(
        { users: [req.user.id, req.query.uid] },
        {
          $setOnInsert: {
            title: user.username,
            users: [req.user.id, req.query.uid],
          },
        },
        { new: true, upsert: true },
        (err, thread) => {
          if (err) throw err;
          return res.json(thread.toJSON());
        },
      );
    })
  }
});

router.get('/thread/:tid', (req, res, next) => {
  Thread.findById(req.params.tid, (err, thread) => {
    if (!thread) return res.sendStatus(404);
    res.json(thread.toJSON())
  })
});

router.post('/thread/:tid/message', (req, res, next) => {
  Thread.addMsg(req.params.tid, {
    from: req.user.id,
    content: req.body.content,
  }).then(res.sendStatus(200))
});

router.get('/thread/:tid/messages', (req, res, next) => {
  Message.find({ threadId: req.params.tid }).limit(20).sort('-createdAt').then((messages) => {
    if (!messages) return res.sendStatus(404);
    res.json({ data: messages.reverse() })
  })
});

router.get('/thread/:tid/users', (req, res, next) => {

});

router.post('/message/:uid', (req, res, next) => {
  // TODO: support multiple user thread
  Thread.findOne({ users: { $in: [req.user.id, req.params.uid] } }, (err, doc) => {
    if (!thread) return res.sendStatus(403);
  })
  return res.sendStatus(200)
});

module.exports = router;
