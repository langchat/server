var router = require('express').Router();

router.use('/api', require('./users'));
router.use('/api', require('./threads'));
router.use('/api', require('./login'));

module.exports = router;