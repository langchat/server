const auth = require('../auth')

const mongoose = require('mongoose');
const passport = require('passport');
const formidable = require('formidable');
const multer = require('multer')
const fs = require('fs');
const path = require('path')
const router = require('express').Router();
const sharp = require('sharp');

const PATH_AVATAR = path.join(__dirname, '../../../public/photos/avatars')
const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, path.join(PATH_AVATAR, 'origin'))
  },
  filename(req, file, cb) {
    cb(null, `${req.user.id}-${file.originalname}`)
  },
})
const upload = multer({ storage });

const User = mongoose.model('User');

router.get('/me', auth.required, (req, res, next) => {
  User.findById(req.user.id).then((user) => {
    if (!user) { return res.sendStatus(401); }
    return res.json({ data: user.toProfileJSON() });
  }).catch(next);
});

router.get('/user', (req, res, next) => {
  if (req.query.username) {
    User.findOne({ username: req.query.username }, (err, user) => {
      if (!user) return res.sendStatus(404);
      return res.json({ data: user.toProfileJSON() });
    }).catch(next);
  } else return res.sendStatus(404)
});

router.get('/user/:uid', (req, res, next) => {
  User.findById(req.params.uid).then((user) => {
    if (!user) { return res.sendStatus(401); }
    return res.json({ data: user.toProfileJSON() });
  }).catch(next);
});

router.post('/me/avatar', upload.single('file'), (req, res, next) => {
  User.findById(req.user.id, (err, user) => {
    // if (user.avatarOrigin) fs.unlink(user.getOriginalAvatarPath(), () => {})
    user.avatarOrigin = `${req.user.id}-${req.file.originalname}`
    user.avatar = true
    console.log(user.getOriginalAvatarPath());
    sharp(user.getOriginalAvatarPath()).resize(100, 100)
      .toBuffer((err, buffer) => {
        console.log(err)
        fs.writeFile(path.join(PATH_AVATAR, `${user._id}.jpg`), buffer, (e) => {
          req.user.avatar = true
        })
      })
    user.save()
  })
});

router.put('/me', (req, res, next) => {
  const { user } = req.body
  if (user.password) delete user.password

  User.findOneAndUpdate(
    { _id: req.user.id },
    {
      $set: user,
    },
  ).then(u => res.json({ data: u.toProfileJSON() }))
  /*
  User.findById(req.user.id).then((user) => {
    if (!user) { return res.sendStatus(401); }

    user = {
      ...user,
      ...req.body.user,
    }

    // only update fields that were actually passed...
    if (typeof req.body.user.username !== 'undefined') {
      user.username = req.body.user.username;
    }
    if (typeof req.body.user.email !== 'undefined') {
      user.email = req.body.user.email;
    }
    if (typeof req.body.user.bio !== 'undefined') {
      user.bio = req.body.user.bio;
    }
    if (typeof req.body.user.image !== 'undefined') {
      user.image = req.body.user.image;
    }
    if (typeof req.body.user.password !== 'undefined') {
      user.setPassword(req.body.user.password);
    }
    console.log(req.body.user)
    return user.save().then(() => {

    });
  }) */
    .catch(next);
});

router.get('/friends', auth.required, (req, res, next) => {
  User.findById(req.user.id).then((user) => {
    if (!user) { return res.sendStatus(401); }
    User.find({ _id: { $in: user.friends } })
      .then(users => res.json({ users }))
  }).catch(next);
});

module.exports = router;
