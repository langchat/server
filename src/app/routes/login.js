const mongoose = require('mongoose');
const passport = require('passport');
const requestPromise = require('request-promise')

const router = require('express').Router();

const User = mongoose.model('User');

router.post('/login', (req, res, next) => {
  if (!req.body.email) {
    return res.status(422).json({ errors: { email: "can't be blank" } });
  }

  if (!req.body.password) {
    return res.status(422).json({ errors: { password: "can't be blank" } });
  }

  passport.authenticate('local', { session: false }, (err, user, info) => {
    if (err) { return next(err); }
    if (user) {
      user.token = user.generateJWT()
      user.save()
      return res.json({ data: user.toAuthJSON() })
    }
    return res.status(422).json(info)
  })(req, res, next)
})

function fetchFacebookProfile(token) {
  const fields = 'email,first_name,last_name,age_range,link,gender,locale,picture,timezone,updated_time,verified'
  console.log(fields)
  return requestPromise({
    uri: 'https://graph.facebook.com/me',
    qs: { access_token: token, fields },
    json: true,
  }).then(res => User.findOne({ email: res.email }).then((user) => {
    if (!user) user = new User({
      username: res.first_name + res.id,
      email: res.email,
    })

    user.token = user.generateJWT()
    user.facebook = {
      id: res.id,
      firstName: res.first_name,
      lastName: res.last_name,
      ageRange: res.age_range,
      timezone: res.timezone,
      updatedTime: res.updated_time,
      verified: res.verified,
      link: res.link,
      gender: res.gender,
    }

    return user.save()
  }))
}

router.get('/auth/facebook', (req, res, next) => {
  fetchFacebookProfile(req.query.token)
    .then(user => res.json({ data: user.toAuthJSON() }))
    .then(next)
})

module.exports = router
