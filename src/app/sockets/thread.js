const io = require('./index')
const request = require('request');
const mongoose = require('mongoose');

const Thread = mongoose.model('Thread')
const Message = mongoose.model('Message')
const User = mongoose.model('User')
const { app } = require('../server')

const getRoomName = (data) => {
  data.language = 'en'
  return `/thread/${data.language}`
}

io.on('connection', (socket) => {
  socket.on('/thread-request/start', (data) => {
    const room = getRoomName(data)

    // Send all users in the room to socket
    if (io.sockets.adapter.rooms[room]) {
      const clients = io.sockets.adapter.rooms[room].sockets
      clients.forEach((client) => {
        socket.emit('/thread-request/new', client.user.toJSON())
      })
    }

    // Send all bots to socket
    User.find({ bot: true, native: data.language }).then((bots) => {
      bots.forEach((bot) => {
        socket.emit('/thread-request/new', bot)
      })
    })

    // Send this user to all others in room
    User.findById(socket.user.id).then((user) => {
      io.of(room).emit('/thread-request/new', user.toJSON())
      socket.join(room)
    })
  })

  socket.on('/thread-request/stop', (data) => {
    const room = getRoomName(data)
    socket.leave(room)
  })
})
