const io = require('./index')
const request = require('request')
const mongoose = require('mongoose')

const Thread = mongoose.model('Thread')
const Message = mongoose.model('Message')
const User = mongoose.model('User')
const { app } = require('../server')

io.on('connection', (socket) => {
  console.log('a user connected');
  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
  socket.on('add_message', (data) => {
    console.log('add_message')
    socket.join(data.threadId);
    Thread.addMsg(data.threadId, {
      from: socket.user.id,
      content: data.content,
    })
  })
});