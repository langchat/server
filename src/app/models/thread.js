const mongoose = require('mongoose');
const languages = require('../resources/languages')
const emitter = require('../emitter')

const Message = mongoose.model('Message');

const ThreadSchema = new mongoose.Schema({
  lang: {
    type: String,
    enum: languages,
  },
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  messages: [{
    uid: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    content: String,
    createdAt: Date,
    seenAt: Date,
  }],
  title: String,
  public: { type: Boolean, default: false },
}, { timestamps: true, usePushEach: true });

ThreadSchema.methods.toJSON = function toJSON() {
  return {
    id: this._id,
    lang: this.lang,
    users: this.users,
    title: this.title,
    public: this.public,
  };
};

ThreadSchema.statics.getByUsers = function getByUsers(uids) {
  return this.findOne({ users: { $in: uids } }, (err, thread) => thread || this.create({
    users: uids,
  }))
}

ThreadSchema.methods.getMessagesJSON = function getMessagesJSON() {
  return this.messages.map(mess => ({
    id: mess._id,
    uid: mess.uid,
    content: mess.content,
    time: mess.createdAt,
  }))
};

ThreadSchema.methods.addUser = function addUser(uid) {
  if (this.users.indexOf(uid) === -1) this.users.push(uid);
  return this.save();
};

ThreadSchema.methods.removeUser = function removeUser(uid) {
  this.users.remove(uid);
  return this.save();
};

ThreadSchema.statics.addMsg = function addMsg(threadId, message) {
  return this.findById(threadId).then((thread) => {
    if (!thread) return;
    // return if user is not in thread
    if (thread.users.indexOf(message.from) < 0) return;

    Message.create({
      ...message,
      threadId: thread._id,
      createdAt: new Date(),
    }).then((msg) => {
      emitter.emit('message:created', { thread, msg })
    })
  });
};

module.exports = mongoose.model('Thread', ThreadSchema);
