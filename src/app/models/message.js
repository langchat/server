const mongoose = require('mongoose')

const MessageSchema = new mongoose.Schema({
  from: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  threadId: { type: mongoose.Schema.Types.ObjectId, ref: 'Thread' },
  content: String,
  createdAt: Date,
  seenAt: Date,
})

MessageSchema.methods.toJSON = function () {
  return {
    id: this._id,
    threadId: this.threadId,
    from: this.from,
    content: this.content,
    time: this.createdAt,
  }
}

MessageSchema.statics.send = function send(uid1, uid2, content) {
  require('./thread')
  const Thread = mongoose.model('Thread')
  return Thread.getByUsers([uid1, uid2]).then((thread) => {
    console.log(uid1)
    const message = new this({
      from: uid1,
      content,
      createdAt: new Date(),
      threadId: thread._id,
    })
    return message.save()
  },)
}

module.exports = mongoose.model('Message', MessageSchema)
