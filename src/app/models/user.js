const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const path = require('path')
const languages = require('../resources/languages')

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    lowercase: true,
    unique: true,
    required: [true, "can't be blank"],
    match: [/^[a-zA-Z0-9]+$/, 'is invalid'],
    index: true,
  },
  email: { type: String, lowercase: true, match: [/\S+@\S+\.\S+/, 'is invalid'] },
  firstName: String,
  lastName: String,
  middleName: String,
  bot: { type: Boolean, default: false },
  bio: String,
  avatar: Boolean,
  avatarOrigin: String,
  friends: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  hash: String,
  salt: String,
  native: { type: String, enum: languages },
  languages: [{ type: String, enum: languages }],
  webhook: String,
  wordCount: Number,
  level: { type: Number, default: 50 },
  levelRange: { type: Number, default: 50 },
  online: Boolean,
  gender: { type: String, enum: ['male', 'female', 'none'] },
  facebook: {
    id: { type: String },
    token: String,
    firstName: String,
    lastName: String,
    gender: { type: String, enum: ['male', 'female', 'none'] },
    locale: String,
    timezone: Number,
    ageRange: {
      min: Number,
    },
    link: String,
    updatedTime: String,
    verified: Boolean,
  },
}, { timestamps: true, usePushEach: true });

UserSchema.plugin(uniqueValidator, { message: 'is already taken.' });

UserSchema.statics.newUser = (username, email, password, fields) => {
  const user = new this({
    ...fields,
    username,
    email,
    friends: [],
  });
  user.setPassword(password);

  user.save((err) => {
    if (err) throw err;
    console.log(`User '${user.username}' created.`);
  });
};

UserSchema.statics.getUsername = function getUsername(id) {
  return this.findById(id, (err, user) => user.username);
};

UserSchema.statics.getByUsername = function getByUsername(username) {
  return this.findOne({ username });
};

UserSchema.statics.getUserId = function getUserId(username) {
  return this.findOne({ username }).then(user => user._id);
};

UserSchema.methods.validPassword = function validPassword(password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

UserSchema.methods.setPassword = function setPassword(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.generateJWT = function generateJWT() {
  const today = new Date();
  const exp = new Date(today);
  exp.setDate(today.getDate() + 60);

  return jwt.sign({
    id: this._id,
    username: this.username,
    exp: parseInt(exp.getTime() / 1000),
  }, process.env.SECRET);
};

UserSchema.methods.getAvatarPath = function getAvatarPath() {
  return `${process.env.STATIC_ROOT}/photos/avatars/${this.avatar ? this._id : 'default'}.jpg`
}

UserSchema.methods.getOriginalAvatarPath = function getAvatarPath() {
  return this.avatarOrigin ? path.join(__dirname, '../../../public/photos/avatars/origin', this.avatarOrigin) : null
}

UserSchema.methods.toJSON = function toJSON() {
  return {
    id: this._id,
    username: this.username,
    bio: this.bio,
    avatar: this.getAvatarPath(),
    native: this.native,
    languages: this.languages,
  };
};

UserSchema.methods.toMinimalJSON = function toMinimalJSON() {
  return {
    id: this._id,
    username: this.username,
    image: this.image,
  };
};

UserSchema.methods.toAuthJSON = function toAuthJSON() {
  return {
    id: this._id,
    username: this.username,
    email: this.email,
    token: this.generateJWT(),
  };
};

UserSchema.methods.toProfileJSON = function toProfileJSON() {
  return {
    id: this._id,
    email: this.email,
    username: this.username,
    online: this.online,
    bio: this.bio,
    avatar: this.getAvatarPath(),
    native: this.native,
    languages: this.languages,
    gender: this.gender,
  };
};

UserSchema.methods.addFriend = function addFriend(uid) {
  if (this.friends.indexOf(uid) === -1) {
    this.friends.push(uid);
  }

  return this.save();
};

UserSchema.methods.removeFriend = function removeFriend(uid) {
  this.friends.remove(uid);
  return this.save();
};

UserSchema.methods.isFriend = function isFriend(id) {
  return this.friends.some(followId => followId.toString() === id.toString());
};

UserSchema.methods.setWordCount = function setWordCount(wordCount) {
  this.wordCount = wordCount
  this.levelRange = 5 + (45 * exp(-wordCount / 10000))
}

module.exports = mongoose.model('User', UserSchema);
