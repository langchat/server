module.exports = {
  User: require('./user'),
  Message: require('./message'),
  Thread: require('./thread')
}