const mongoose = require('./connect');

mongoose.set('debug', false);
mongoose.Promise = require('bluebird');

const User = mongoose.model('User');
const Thread = mongoose.model('Thread');
const Message = mongoose.model('Message');

const readline = require('readline');

const rl = readline.createInterface(process.stdin, process.stdout);

const utils = require('./utils');


switch (process.argv[2]) {
  case 'ls':
    utils.list(Message, ['from', 'content', 'createdAt'], [27, 50, 50]).then(process.exit);
    break;
  case 'send':
    User.getUserId(process.argv[3])
      .then(uid1 => User.getUserId(process.argv[4]).then(uid2 => [uid1, uid2]))
      .then(uids => Message.send(uids[0], uids[1], process.argv[5]))
      .then(() => {
        console.log('Message sent.');
        process.exit(0);
      });
    break;
  case 'rm':
    User.remove({ username: process.argv[3] }).then(console.log('User deleted'));
    break;
  case 'get':
    User.findOne({ username: process.argv[3] }).then((user) => {
      console.log(user.toJSON());
      process.exit(0);
    });
    break;
  case 'set':
    User.findOne({ username: process.argv[3] }).then((user) => {
      user[process.argv[4]] = process.argv[5];
      user.save().then(process.exit);
    });
    break;
  default:
    console.log('Command not found.');
    process.exit();
}
