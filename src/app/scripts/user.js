const mongoose = require('./connect');

mongoose.set('debug', false);
mongoose.Promise = require('bluebird');

const User = mongoose.model('User');
const Thread = mongoose.model('Thread');
const Message = mongoose.model('Message');

const utils = require('./utils')

switch (process.argv[2]) {
  case 'ls':
    utils.list(User, ['_id', 'bot', 'username', 'email'], [27, 10, 20, 25]).then(process.exit);
    break;
  case 'add':
    User.create({ username: process.argv[3] })
      .then((user) => {
        console.log(`User ${user.username} added`);
        process.exit(0);
      });
    break;
  case 'rm':
    User.remove({ username: process.argv[3] }).then(console.log('User deleted'));
    break;
  case 'get':
    User.findOne({ username: process.argv[3] }).then((user) => {
      console.log(user);
      process.exit(0)
    });
    break;
  case 'set':
    User.findOne({ username: process.argv[3] }).then((user) => {
      if (process.argv[4] === 'password') {
        user.setPassword(process.argv[5])
      } else {
        user[process.argv[4]] = process.argv[5]
      }
      return user.save().then(process.exit)
    });
    break;
  default:
}
