const mongoose = require('./connect');

mongoose.set('debug', false);
mongoose.Promise = require('bluebird');

const User = mongoose.model('User');
const Thread = mongoose.model('Thread');
const Message = mongoose.model('Message');

const readline = require('readline');

const rl = readline.createInterface(process.stdin, process.stdout);

const utils = require('./utils')


switch (process.argv[2]) {
  case 'ls':
    utils.list(Thread, ['_id', 'users', 'createdAt', 'title'], [27, 50, 30, 10]).then(process.exit);
    break;
  case 'new':
    User.create({ username: process.argv[3] })
      .then((user) => {
        console.log(`User ${user.username} added`);
        process.exit(0);
      });
    break;
  case 'rm':
    Thread.remove({ _id: process.argv[3] }).then(console.log('Deleted')).then(process.exit);
    break;
  case 'get':
    Thread.findById(process.argv[3]).then((thread) => {
      
      console.log(thread.toJSON())
      process.exit(0)
    });
    break
  case 'set':
    User.findOne({ username: process.argv[3] }).then((user) => {
      user[process.argv[4]] = process.argv[5]
      return user.save().then(process.exit(0))
    });
    break
  default:
    break;
}
