const Table = require('cli-table');

module.exports = {
  list: (schema, fields, columnWidths) => {
    const table = new Table({
      chars: {
        top: '',
        'top-mid': '',
        'top-left': '',
        'top-right': '',
        bottom: '',
        'bottom-mid': '',
        'bottom-left': '',
        'bottom-right': '',
        left: '',
        'left-mid': '',
        mid: '',
        'mid-mid': '',
        right: '',
        'right-mid': '',
        middle: ' ',
      },
      head: fields,
      colWidths: columnWidths,
    });

    return schema.find({}).limit(10).then(objs =>
      objs.forEach(obj =>
        table.push(fields.map(field => obj[field] || 'None'))))
      .then(() => console.log(table.toString()));
  },
}
