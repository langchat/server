const mongoose = require('mongoose');
require('../config')
require('../models');
mongoose.connect(process.env.MONGODB_URI);
mongoose.set('debug', true);

module.exports = mongoose