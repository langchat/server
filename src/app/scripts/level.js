const mongoose = require('./connect');

mongoose.set('debug', false);
mongoose.Promise = require('bluebird');

const User = mongoose.model('User');
const Thread = mongoose.model('Thread');
const Message = mongoose.model('Message');

//const utils = require('./utils')

/*
switch (process.argv[2]) {
  case 'refresh':
    User.getByUsername(process.argv[3]).then((user) => {
      Message.find({ from: user._id }).then(messages => messages.map(msg => msg.content))
        .then((msgs) => {
          //const wordCount = langutils.countWords(msgs.join('\n'))
          return user.save({
            ...user,
            wordCount,
          })
        }).then(process.exit)
    })
    break;
  default:
}
*/
