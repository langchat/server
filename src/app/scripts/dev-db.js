const mongoose = require('./connect');
mongoose.Promise = require('bluebird');

const User = mongoose.model('User');

User.remove({})
  .then(Promise.all([
    User.newUser('admin', 'admin@example.com', '123456', { bot: false }),
    User.newUser('trungdv', 'viettrungdang@gmail.com', '123456', { bot: false })
  ]))
  .then(process.exit(0));
