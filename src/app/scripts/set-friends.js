const mongoose = require('./connect')
require('../models/user');
var User = mongoose.model('User');
mongoose.Promise = require('bluebird');

User.findOne({ username: process.argv[2] }, (err, user1) => {
  User.findOne({ username: process.argv[3] }, (err, user2) => {
    Promise.all([user1.addFriend(user2._id), user2.addFriend(user1._id)]).then((err, res) => {
      console.log(`${process.argv[2]} and ${process.argv[3]} are now friends.`)
      process.exit(0)
    })
  })
});