require('./config')

const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const mongoose = require('mongoose')
const errorhandler = require('errorhandler')
const cors = require('cors')
const auth = require('./auth')
const jwt = require('jsonwebtoken')
const path = require('path')

const app = express(); module.exports.app = app;

let isProduction = process.env.NODE_ENV === 'production';

console.log(process.env.MONGODB_URI)
mongoose.connect(process.env.MONGODB_URI);

if (!isProduction) {
  // mongoose.set('debug', true)
}

require('./models/index');
require('./config/passport')


// App
app.use(cors())
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride('_method'));
app.use('/public', express.static(path.join(__dirname, '../../public')))
app.use(auth.optional)
app.use(require('./routes/index'));

if (!isProduction) app.use(errorhandler());

//app.get('/', (req, res) => {
//  res.send('Hello world\n');
//});

app.get('/api', (req, res) => {
  res.send('API is running');
});

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (!isProduction) {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

app.use((err, req, res, next) => {
  console.log(res)
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

const server = require('http').Server(app)
const io = require('socket.io')(server);
module.exports.server = server;
module.exports.io = io;

require('./sockets')
//const io = require('./sockets')

io.use((socket, next) => {
  if (socket.handshake.query.token) {
    jwt.verify(socket.handshake.query.token, process.env.SECRET, (err, decoded) => {
      if (err) { socket.disconnect(err); return next(err) }
      if (!decoded) { socket.disconnect('invalid token'); return next(Error('invalid token')) }
      return mongoose.model('User').findById(decoded.id).then((user) => {
        socket.user = user
        next()
      })
    })
  }
});

require('./sockets')
require('./events/index')

server.listen(process.env.PORT || 3000, () => {
  //console.log(`Running on http://${process.env.HOST}:${server.address().port}`);
});

