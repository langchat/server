const dotenv = require('dotenv')

if (process.env.NODE_ENV === 'production') {
  dotenv.config({ path: `${__dirname}/prod.env` })
} else { // if (process.env.NODE_ENV === 'production') {
  dotenv.config({ path: `${__dirname}/dev.env` })
}
