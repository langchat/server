const { io } = require('../server')
const { User } = require('../models/index')
const request = require('request')
const emitter = require('../emitter')

emitter.on('message:created', (data) => {
  const { thread, msg } = data
  console.log(`message:created (${msg.from}: ${msg.content})`)

  io.to(thread._id).emit('message_added', msg);

  // send socket message to users in thread
  thread.users.forEach(userId => User.findById(userId, (err, user) => {
    if (user._id.equals(msg.from)) return;
    if (user.bot) {
      try {
        request.post(
          user.webhook,
          {
            json: {
              thread_id: msg.threadId,
              from: msg.from,
              content: msg.content,
              createdAt: msg.createdAt,
            },
          },
          (error, res, body) => {}
        );
      } catch (e) {}
    }
  }))

  // refresh level
  User.findById(msg.from).then((user) => {
    //user.wordCount += langutils.countWords(msg.content);
    user.save()
  })
})

