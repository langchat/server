import sys
import numpy as np
from nltk.corpus import wordnet as wn
#import nltk
#nltk.download("wordnet")
#print(wn.synsets('contemporary'))
#print(wn.synset('contemporary.n.01').definition())

from en.score import EnglishScorer
score = EnglishScorer()
print(score.score(sys.argv[1]))