import csv
import db
import numpy as np
import spacy
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet as wn
#nltk.download('wordnet')

def wordfrequency():
    with open('../,,/data/wordfrequency.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        words = {}
        next(reader)
        for word in reader:
            words[word[1]] = int(word[3])
        max_freq = max([value for key, value in words.items()])
        print(max_freq)
        words = { key: value / max_freq for key, value in words.items() }
        #words = words / 
        db.cache.hmset('words', words)
        #print(words)

def gg20000():
    wordnet_lemmatizer = WordNetLemmatizer()
    unscored_words = open('../../data/unscored_words.dat').read().split('\n')
    common_words = open('../../data/common_words.dat').read().split('\n')
    f = open('../../data/google-10000-english/20k.txt', 'r')
    count = 0
    words = {}
    for word in common_words:
        words[word] = 1
    for word in unscored_words: 
        words[word] = 0

    for word in f.readlines():
        word = wordnet_lemmatizer.lemmatize(word)
        if word not in words: 
            count += 1
            words[word] = 2

    for ss in wn.all_synsets():
        word = ss.lemma_names()[0]
        if word not in words:
            count += 1
            words[word] = 2
            #print(word)
    
    print(len(words))
    db.cache.hmset('words', words)

nlp = spacy.load('en')
gg20000()