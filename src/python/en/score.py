import sys
import json
import spacy
import numpy as np
import db

class EnglishScorer:
    nlp = spacy.load('en')
    DEFAULT_WORDS = {'-PRON-': -1, '.': -1, ',': -1}
    def __init__(self):
        db.cache.hmset('words', self.DEFAULT_WORDS)
    
    def score(self, text):
        #print(db.cache.hmget('words', 'disappear'))
        #exit()

        #word_count = db.cache.hlen('words')
        level_max = 2
        doc = [token.lemma_ for token in self.nlp(text)]
        print(doc)

        #print(doc[5].lemma)
        word_freqs = db.cache.hmget('words', doc)

        score = 0
        score = np.array([float(f) if f else -1 for f in word_freqs])
        score = score[score >= 0]
        levels = [(score == i).sum() / len(score) for i in range(level_max + 1)]
        #cutoff = 0.2
        #score = (score[score >= cutoff] - cutoff) * (1 / (1 - cutoff))
        #score = score[score > 0]
        #print(levels)
        levels = (levels * np.array([0, 1.5, 4]))
        #score = score ** 2

        #return np.average(score) if len(score) > 0 else 0
        return min(np.sum(levels), 1.0)