FROM node:latest

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y python python-pip python-dev


WORKDIR /code
COPY package*.json ./
RUN npm install
COPY ./src ./src

RUN pip install -r ./src/python/requirements.txt

EXPOSE 3000